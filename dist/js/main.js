// Smooth scrolling
const scroll = new SmoothScroll('a[href*="#"]', {
  speed: 800,
});

// Dark themed recaptcha
document.addEventListener("DOMContentLoaded", (event) => {
  const recaptcha = document.querySelector(".g-recaptcha");
  recaptcha.setAttribute("data-theme", "dark");
});
